'''
1) Desarrolle una función que calcule y muestre el promedio de 4 notas de un estudiante
2) Desarrolle una función que reciba como parámetro el nombre del usuario y ciudad, muestre
    por consola: "Hola nombre_usuario, nos alegra que nos visites desde nombre_ciudad"
3) Desarrolle una función que eleve a la potencia un número
4) Desarrolle una función que muestre el valor del interés a ganar en un CDT:
    Fórmula: (cantidad * porcentaje_interes * tiempo) / 12
5) Desarrolle un función que muestre el TOTAL DEL DINERO a retirar en un CDT:
    NOTA: 
        *Utilice la misma fórmula del punto 4
        *El parámetro que representa el PORCENTAJE DEL INTERÉS se debe 
        recibir como entero para que internamente en la función se convierta a float.
        Ejemplo:
            30 -> 0.3
            10 -> 0.1
            50 -> 0.5

'''
#Punto 1
def promedio_notas(nota_1, nota_2, nota_3, nota_4):
    promedio = (nota_1 + nota_2 + nota_3 + nota_4) / 4
    print(promedio)

#promedio_notas(3.5, 4, 4.8, 3)
#Punto 2
def saludar(nombre_usuario, nombre_ciudad):
    print(f"Hola {nombre_usuario}', nos alegra que nos visites desde {nombre_ciudad}")

saludar('Juan', 'Barranquilla')

#Punto 3
#Karol Cardenas
def potencia(numero, elevado):
    operacion = numero ** elevado
    print (f"EL RESULTADO ES: {operacion}")

#Punto 4
def valor_interes(cantidad, porcentajeInteres, tiempo):
    interesTotal = (cantidad*porcentajeInteres*tiempo) /12
    print("El valor del interes para este CDT es: ", interesTotal)

#Punto 5
#Solución de Valeria
def totalDinero(cantidad, porcentajeInteres, tiempo):
    interesTotal = (cantidad*(porcentajeInteres/100)*tiempo) /12
    print("El valor del interes para este CDT es: ", interesTotal)
    print("Obteniendo un total disponible de: ", interesTotal+cantidad)


print("------------------------\nRETOMAMOS A LAS 9:00AM\n------------------------")