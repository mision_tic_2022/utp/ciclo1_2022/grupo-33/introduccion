
#Condicionales
var_1 = 100
var_2 = 150

if var_1 > var_2:
    print("Var_1 es mayor que var_2")
else:
    print("Var_1 es menor que var_2")

print("Aquí sigue otro proceso por fuera del if - else")

num_1 = 100
num_2 = 200
num_3 = 50
num_4 = 120
#not -> negacion
if (num_1 > num_3) and (num_1 > num_4):
    print("Cumple la condicion del AND")
else:
    print("No cumple la condición del AND")


edad_persona = 16
'''
Un solo igual (=) es asignación
Un doble igual (==) es comparativo
'''
if edad_persona >= 18:
    print('Es mayor de edad')
else:
    print('Es menor de edad')

#diferente que ->  !=
if edad_persona != 16:
    print("Es diferente a 16")
