'''FUNCIONES'''

#def -> palabra reservada para crear una función

#Función para mostrar un mensaje
def miFuncion():
    mensaje = "Hola mundo, esta es mi primera función"
    print(mensaje)

def sumar():
    num_1 = 10
    num_2 = 20
    resultado = num_1 + num_2
    print(f"{num_1} + {num_2} = {resultado}")

#Función con parámetros
def sumar_con_parametros(num_1: float, num_2: float, num_3: float):
    resultado = num_1 + num_2 + num_3
    print("Resultado = %.2f" %resultado)


#Llamar la dunción
#miFuncion()
#sumar()
#sumar_con_parametros(5, 20, 10)


#-------------FUNCIONES CON RETURN---------------

def sumar(n1, n2, n3):
    #Variable local de la función
    sum = n1 + n2 + n3
    return sum

resultado = sumar(10,20,30)

resultado_2 = sumar(resultado, 50, 60)
print(resultado_2)

def saludar(nombre: str, ciudad: str):
    return "Hola "+nombre+", nos alegra que nos visites desde "+ciudad
    #return f"Hola {nombre}, nos alegra que nos visites desde {ciudad}"

print( saludar('Juan', 'Medellín') )



'''
1) Desarrolle una función que determine si un número es par o impar
2) Desarrolle una función que retorne si una persona es mayor de edad o no
3) Desarrolle una función que retorne el resultado de la siguiente fórmula:
    valor_interes = (cantidad * valor_interes * tiempo) / 12

#https://paste.ofcode.org/X69c9RsL8Ugqyj2VfnrutZ
'''


#Punto 1
def esPar(numero):
    par = False
    if numero % 2 == 0:
        par = True
    return par

if esPar(4):
    print("Ejecuto un proceso")

#Punto 2
def deternar_mayor_edad(edad):
    mensaje = ''
    if edad >= 18:
        mensaje = 'Es mayor de edad'
    else:
        mensaje = 'Es menor de edad'
    return mensaje

print( deternar_mayor_edad(18) )

#Punto 3
def CDT(cantidad, valor_interes, tiempo):
    return ((cantidad * valor_interes * tiempo) / 12)

print( CDT(1000000, 0.3, 5) )











