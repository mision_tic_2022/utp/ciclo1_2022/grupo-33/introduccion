#Esto es un comentario de una sola linea
'''
Esto es un comentario
de varias
lineas
'''

"""
Con comilla doble también
se comenta
varias lineas
"""

#Esto es una variable de tipo entero
numero = 10
#Mostrar el número en consola
print(numero)

#Mostrar mensaje por consola
print("Hola mundo")

'''-----------Variables-------'''
#Variables de tipo entero (números que no tienen decimales)
numero = 10
miVariable = 20
num_1 = 5
#Indicar el tipo de dato solo se hace para que el código sea LEGIBLE
#pero no lo convierte en fuertemente tipado
num_2: int = 10

#Variables de tipo flotante (números con decimales)
flotante_1 = 3.141598
flotante_2: float = 20.22

#Variables de tipo String (texto)
anio = "2022"
mensaje: str = 'Esto es un String'
direccion = "Cra 100 #21-22"


#Operaciones matemáticas básicas
suma = num_1 + num_2
multiplicacion = flotante_1 * num_2
division = multiplicacion / num_1 

#Elevado a la potencia (doble signo de multiplicar '**')
elevado = num_1 ** 2
#Módulo; retorna el residuo de una división
modulo = num_1 % 2

#Mostrar resultados

#Concatenar en el PRINT con coma
print('Suma = ', suma)
#Formatear la salida limitando el número de flotantes (decimales) a mostrar
print("Multiplicación = %.2f" %multiplicacion)
#Sin limitar los números decimales
print("Multiplicación = %f" %multiplicacion)
#Dar formato a la salida #2
print(f"División = {division}")

'''---------String (Texto)---------'''
nombre = "Andrés"
apellido = 'Quintero'

nombre_completo = nombre + ' ' + apellido

print(nombre_completo)





